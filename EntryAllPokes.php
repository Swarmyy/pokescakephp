<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        .black {
            background: #808080
        }
    </style>

    <?php
    $host = "localhost";
    $base = "introtpcakephp";
    $port = 3306;
    $login = "root";
    $passe = "root";

    //mise en place des erreurs PDO : gestion des exceptions
    function exception_handler_perso($exception)
    {
        print "Une exception a &eacute;t&eacute; lanc&eacute;e !!!";
        print "<pre>";
        var_dump($exception);
        print "</pre>";
    }

    set_exception_handler('exception_handler_perso');

    $db = new PDO("mysql:host=$host;port=$port;dbname=$base", $login, $passe);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->query("SET NAMES UTF8");
    ?>
</head>

<body>
    <?php
    ini_set("allow_url_fopen", 1);
    $json = file_get_contents('https://pokeapi.co/api/v2/pokemon?limit=964');
    $jsonPokes = json_decode($json, true);

    $indexKeep = array(
        "is_default",
        "id",
    );
    $indexDefaults = array(
        "encounters" => 0,
        "learn_method" => 1,
        "name" => 2,
        "url" => 3,
        "sprites" => 4,
        "stats" => 5,
        "types" => 6,
        "69" => 7
    );

    foreach ($jsonPokes["results"] as $i => $value) {
        $json = file_get_contents("{$value["url"]}");
        $obj = json_decode($json, true);
        $isDefault = array();
        foreach ($obj as $pokelem) {
            if (in_array(array_search($pokelem, $obj), $indexKeep)) {
                switch (array_search($pokelem, $obj)) {
                    case "is_default":
                        array_push($isDefault, $pokelem);
                        break;
                    case "id":
                        break;
                    default:
                }
            }
        }

        /***************************************************************************************************
         * EG:
         * 1
         * bulbasaur
         * Array ( 
         *  [speed] => 45 
         *  [special-defense] => 65 
         *  [special-attack] => 65 
         *  [defense] => 49 
         *  [attack] => 49 
         *  [hp] => 45 ) 
         * Array ( 
         *  [back] => https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png 
         *  [front] => https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png )
         */
        $url = explode('/', $isDefault[$indexDefaults["url"]]["url"]);
        $id = $url[count($url) - 2];
        $name = $isDefault[$indexDefaults["url"]]["name"];
        $stats = array();
        foreach ($isDefault[$indexDefaults["stats"]] as $stat) {
            $stats[$stat["stat"]["name"]] = $stat["base_stat"];
        }
        $sprites = array(
            "back" => $isDefault[$indexDefaults["sprites"]]["back_default"],
            "front" => $isDefault[$indexDefaults["sprites"]]["front_default"]
        );
        $rq = $db->prepare("SELECT FROM pokes where id=:id");
        $rq->execute(array(":id" => $id));
        if ($rq->rowCount() == 0) {
            $rq = $db->prepare("INSERT INTO pokes VALUES(:id,:name,:hp,:atk,:def,:spd,:URLF,:URLB)");
            $rq->execute(array(":id" => $id, ":name" => $name, ":hp" => $stats["hp"], ":atk" => $stats["attack"], ":def" => $stats["defense"], ":spd" => $stats["speed"], ":URLF" => $sprites["front"], ":URLB" => $sprites["back"]));
        }
    }
    echo "done";
    ?>
</body>

</html>