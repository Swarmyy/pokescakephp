<?php
use Migrations\AbstractMigration;

class AddFavorisToDresseurPokes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('dresseur_pokes');
        $table->addColumn('is_fav', 'boolean', [
            'default' => false,            
        ]);
        $table->update();
    }
}
