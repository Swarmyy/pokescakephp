<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DresseursPokesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DresseursPokesTable Test Case
 */
class DresseursPokesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DresseursPokesTable
     */
    public $DresseursPokes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DresseursPokes',
        'app.Dresseurs',
        'app.Pokes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DresseursPokes') ? [] : ['className' => DresseursPokesTable::class];
        $this->DresseursPokes = TableRegistry::getTableLocator()->get('DresseursPokes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DresseursPokes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
