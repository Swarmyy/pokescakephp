<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FightTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FightTable Test Case
 */
class FightTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FightTable
     */
    public $Fight;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Fight',
        'app.FirstDresseurs',
        'app.SecondDresseurs',
        'app.WinnerDresseurs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fight') ? [] : ['className' => FightTable::class];
        $this->Fight = TableRegistry::getTableLocator()->get('Fight', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fight);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
