<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $form_data = $this->request->getData();
            if ($form_data['first_dresseur_id'] != $form_data['second_dresseur_id']) {
                $form_data['winner_dresseur_id'] = $this->_fight($form_data);
                if($form_data['winner_dresseur_id']=="error"){
                    return;
                }
                if ($form_data['winner_dresseur_id']) {
                    $fight = $this->Fights->patchEntity($fight, $form_data);
                    if ($this->Fights->save($fight)) {
                        $this->Flash->success(__('The fight has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The fight could not be saved. Please, try again.'));
                }
            }else{
                $this->Flash->error(__('Vous ne pouvez pas vous battre contre vous-même !'));
            }
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    protected function _calculDamage($Niv, $Att, $Pui, $Def)
    {
        $CM = (mt_rand() / mt_getrandmax()) * 0.15 + 0.85;
        return (((($Niv * 0.4 + 2) * $Att * $Pui) / ($Def * 50)) + 2) * $CM;
    }

    protected function _fight($formData)
    {
        $dresseur1 = $this->getPokes($formData['first_dresseur_id']);
        $dresseur2 = $this->getPokes($formData['second_dresseur_id']);
        if(!(isset($dresseur1)&&isset($dresseur2))){
            if(!isset($dresseur1)&&!isset($dresseur2)){
                $this->Flash->error(__('Aucun pokemons pour les 2 dresseurs'));
                return "error";
            }
            if(!isset($dresseur1)){
                $this->Flash->error(__('Pas de pokemons pour le premier combattant, le second gagne par défaut.'));
                return $formData['second_dresseur_id'];
            }
            if(!isset($dresseur2)){
                $this->Flash->error(__('Pas de pokemons pour le second combattant, le premier gagne par défaut.'));
                return $formData['second_dresseur_id'];
            }
        }
        Log::info($dresseur1);
        Log::info($dresseur2);
        while (!($dresseur1[0]['HP'] <= 0 || $dresseur2[0]['HP'] <= 0)) {
            $dresseur1[0]['HP'] -= $this->_calculDamage(1, $dresseur1[0]['ATT'], 1, $dresseur1[0]['DEF']);
            $dresseur2[0]['HP'] -= $this->_calculDamage(1, $dresseur2[0]['ATT'], 1, $dresseur2[0]['DEF']);
        }
        if ($dresseur2[0]['HP'] <= 0) {
            return $formData['first_dresseur_id'];
        }
        if ($dresseur1[0]['HP'] <= 0) {
            return $formData['second_dresseur_id'];
        }
    }

    protected function getPokes($id_trainer)
    {
        $output = array();
        $output_id = array();
        $outputtemp = array();
        $compt = 0;
        $query = TableRegistry::getTableLocator()->get('DresseurPokes')->find()->select(['poke_id', 'is_fav'])->where(['dresseur_id =' => $id_trainer]);
        foreach ($query as $data) {
            if ($data['is_fav'] == true) {
                $outputtemp = array();
                $outputtemp[0] = $data['poke_id'];
                for ($i = 1; $i <= count($output_id); $i++) {
                    $outputtemp[$i] = $output_id[$i - 1];
                }
                $output_id = $outputtemp;
            } else {
                $output_id[$compt] = $data['poke_id'];
            }
            $compt++;
        }
        $compt = 0;
        for ($i = 0; $i < count($output_id); $i++) {
            $query = TableRegistry::getTableLocator()->get('Pokes')->find()->select(['Name', 'attack', 'speed', 'health', 'defense'])->where(['id =' => $output_id[$i]]);
            foreach ($query as $data) {
                $poke = [
                    "name" => $data->Nom,
                    "HP" => $data->health,
                    "ATT" => $data->attack,
                    "DEF" => $data->defense,
                    "Speed" => $data->speed,
                ];
                $output[$compt] = $poke;
                $compt++;
            }
        }
        return $output;
    }
}
