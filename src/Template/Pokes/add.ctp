<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokes'), ['controller' => 'DresseurPokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Poke'), ['controller' => 'DresseurPokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pokes form large-9 medium-8 columns content">
    <?= $this->Form->create($poke) ?>
    <fieldset>
        <legend><?= __('Add Poke') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('health');
            echo $this->Form->control('attack');
            echo $this->Form->control('defense');
            echo $this->Form->control('speed');
            echo $this->Form->control('URLSpriteFront');
            echo $this->Form->control('URLSpriteBack');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
