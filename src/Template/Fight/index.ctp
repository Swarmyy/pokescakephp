<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight[]|\Cake\Collection\CollectionInterface $fight
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Fight'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fight index large-9 medium-8 columns content">
    <h3><?= __('Fight') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_dresseur_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('second_dresseur_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('winner_dresseur_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($fight as $fight): ?>
            <tr>
                <td><?= $this->Number->format($fight->id) ?></td>
                <td><?= $this->Number->format($fight->first_dresseur_id) ?></td>
                <td><?= $this->Number->format($fight->second_dresseur_id) ?></td>
                <td><?= $this->Number->format($fight->winner_dresseur_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $fight->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fight->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
