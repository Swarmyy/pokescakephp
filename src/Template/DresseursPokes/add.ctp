<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke $dresseursPoke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Dresseurs Pokes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseursPokes form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseursPoke) ?>
    <fieldset>
        <legend><?= __('Ajouter Dresseur de pokémon') ?></legend>
        <?php
            echo $this->Form->control('dresseur_id', ['options' => $dresseurs]);
            echo $this->Form->control('poke_id', ['options' => $pokes]);
            echo $this->Form->control('favorite');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Valider')) ?>
    <?= $this->Form->end() ?>
</div>
