<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke $dresseursPoke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseurs Poke'), ['action' => 'edit', $dresseursPoke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseurs Poke'), ['action' => 'delete', $dresseursPoke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseursPoke->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs Pokes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseurs Poke'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseursPokes view large-9 medium-8 columns content">
    <h3><?= h($dresseursPoke->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseursPoke->has('dresseur') ? $this->Html->link($dresseursPoke->dresseur->id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseursPoke->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Poke') ?></th>
            <td><?= $dresseursPoke->has('poke') ? $this->Html->link($dresseursPoke->poke->id, ['controller' => 'Pokes', 'action' => 'view', $dresseursPoke->poke->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseursPoke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseursPoke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseursPoke->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Favorite') ?></th>
            <td><?= $dresseursPoke->favorite ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
