<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pokes Model
 *
 * @property \App\Model\Table\DresseurPokesTable&\Cake\ORM\Association\HasMany $DresseurPokes
 *
 * @method \App\Model\Entity\Poke get($primaryKey, $options = [])
 * @method \App\Model\Entity\Poke newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Poke[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Poke|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Poke saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Poke patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Poke[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Poke findOrCreate($search, callable $callback = null, $options = [])
 */
class PokesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pokes');
        $this->setDisplayField('nom');
        $this->setPrimaryKey('id');

        $this->hasMany('DresseurPokes', [
            'foreignKey' => 'poke_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('health')
            ->requirePresence('health', 'create')
            ->notEmptyString('health');

        $validator
            ->integer('attack')
            ->requirePresence('attack', 'create')
            ->notEmptyString('attack');

        $validator
            ->integer('defense')
            ->requirePresence('defense', 'create')
            ->notEmptyString('defense');

        $validator
            ->integer('speed')
            ->requirePresence('speed', 'create')
            ->notEmptyString('speed');

        $validator
            ->scalar('URLSpriteFront')
            ->requirePresence('URLSpriteFront', 'create')
            ->notEmptyString('URLSpriteFront');

        $validator
            ->scalar('URLSpriteBack')
            ->requirePresence('URLSpriteBack', 'create')
            ->notEmptyString('URLSpriteBack');

        return $validator;
    }
}
