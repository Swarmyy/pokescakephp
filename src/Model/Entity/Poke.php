<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Poke Entity
 *
 * @property int $id
 * @property string $name
 * @property int $health
 * @property int $attack
 * @property int $defense
 * @property int $speed
 * @property string $URLSpriteFront
 * @property string $URLSpriteBack
 *
 * @property \App\Model\Entity\DresseurPoke[] $dresseur_pokes
 */
class Poke extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'health' => true,
        'attack' => true,
        'defense' => true,
        'speed' => true,
        'URLSpriteFront' => true,
        'URLSpriteBack' => true,
        'dresseur_pokes' => true
    ];
}
